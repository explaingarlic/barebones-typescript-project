# Barebones typescript projest.

`npm start` to get started.

Change root directory (where your index.ts file is, most likely) in ```tsconfig.json``` and in ```package.json``` for dev nodemon dependency.

Be sure to `git remote remove origin`

## TODOs:

- Add barebones Typescript CI pipeline.

- Perhaps integrate with barebones k8s deployment CI/CD pipeline?
